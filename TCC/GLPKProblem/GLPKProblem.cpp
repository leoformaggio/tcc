//
//  GLPKProblem.cpp
//  TCC
//
//  Created by Leonardo Formaggio on 9/18/11.
//  Copyright 2011 Fingertips. All rights reserved.
//

#include "GLPKProblem.h"
#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

#pragma mark - Constructors

GLPKProblem::GLPKProblem() {
    
}

GLPKProblem::GLPKProblem(int _rows, int _columns) {
    rows = _rows;
    columns = _columns;
    
    problem = glp_create_prob();
    
    glp_init_smcp(&parm);
    parm.msg_lev = GLP_MSG_OFF;
//	parm.presolve = GLP_ON;
    
    // Adiciona as restricoes (linhas)
    glp_add_rows(problem, rows);
    
    // Adiciona as variaveis (colunas)
    glp_add_cols(problem, columns);
}

#pragma mark - Destructor

GLPKProblem::~GLPKProblem() {
    // Deleta o problema
    glp_delete_prob(problem);
}

#pragma mark - GLPK Problem Methods

void GLPKProblem::setProblemName(string name) {
    // Define um nome para o problema
    glp_set_prob_name(problem, name.c_str());
}

void GLPKProblem::setOptimizationDirection(int direction) {
    // Define o tipo do problema (maximizacao/minimizacao)
    glp_set_obj_dir(problem, direction);
}

void GLPKProblem::loadMatrix(double **matrix) {
    int *ia = (int *)malloc((1+rows*columns)*sizeof(int));
    int *ja = (int *)malloc((1+rows*columns)*sizeof(int));
    double *ar = (double *)malloc((1+rows*columns)*sizeof(double));
    
    // Define a matriz de restricoes
    int count = 1;
    for (int i=1; i <= rows; i++) {
        for (int j=1; j <= columns; j++) {
            ia[count] = i;
            ja[count] = j;
            ar[count] = matrix[i][j];
            count++;
        }
    }
    
    // Carrega o problema
    glp_load_matrix(problem, rows*columns, ia, ja, ar);
    
    free(ia);
    free(ja);
    free(ar);
}

void GLPKProblem::setMatrixEntry(int row, int column, double value) {    
    if (glp_get_num_cols(problem) < glp_get_num_rows(problem)) {

        int length = glp_get_mat_row(problem, row, 0, 0);

        vector<int> indexes(length + 2);
        vector<double> values(length + 2);

        glp_get_mat_row(problem, row, &indexes.front(), &values.front());

        //The following code does not suppose that the elements of the
        //array indexes are sorted
        bool found = false;
        for (int i = 1; i  <= length; ++i) {
            if (indexes[i] == column) {
                found = true;
                values[i] = value;
                break;
            }
        }
        if (!found) {
            ++length;
            indexes[length] = column;
            values[length] = value;
        }

        glp_set_mat_row(problem, row, length, &indexes.front(), &values.front());

    } else {

        int length = glp_get_mat_col(problem, column, 0, 0);

        vector<int> indexes(length + 2);
        vector<double> values(length + 2);

        glp_get_mat_col(problem, column, &indexes.front(), &values.front());

        //The following code does not suppose that the elements of the
        //array indexes are sorted
        bool found = false;
        for (int i = 1; i <= length; ++i) {
            if (indexes[i] == row) {
                found = true;
                values[i] = value;
                break;
            }
        }
        if (!found) {
            ++length;
            indexes[length] = row;
            values[length] = value;
        }

        glp_set_mat_col(problem, column, length, &indexes.front(), &values.front());
    }
}

void GLPKProblem::setObjectiveFunction(const double *coef) {
    // Define os respectivos coeficientes na FO
    for (int i=1; i <= columns; i++) {
        glp_set_col_bnds(problem, i, GLP_LO, 0.0, 0.0);
        glp_set_obj_coef(problem, i, coef[i]);
    }
}

void GLPKProblem::setColumnBounds(int _column, int type, double lowerBound, double upperBound) {
    glp_set_col_bnds(problem, _column, type, lowerBound, upperBound);
}

void GLPKProblem::setColumnBounds(int start, int end, int type, double lowerBound, double upperBound) {
	for (int column=start; column <= end; column++) {
		glp_set_col_bnds(problem, column, type, lowerBound, upperBound);
	}
}

void GLPKProblem::setRowBounds(int _row, int type, double lowerBound, double upperBound) {
    glp_set_row_bnds(problem, _row, type, lowerBound, upperBound);
}

void GLPKProblem::setRowBounds(int start, int end, int type, double lowerBounds, double upperBounds) {
    for (int row=start; row <= end; row++) {
        glp_set_row_bnds(problem, row, type, lowerBounds, upperBounds);
    }
}

void GLPKProblem::setRowBounds(int start, int end, int type, double *lowerBounds, double *upperBounds) {
    int i;
    for (int row=start; row <= end; row++) {
        i = row - start + 1;
        glp_set_row_bnds(problem, row, type, lowerBounds[i], upperBounds[i]);
    }
}

int GLPKProblem::getRowStatus(int row) {
    return glp_get_row_stat(problem, row);
}

double GLPKProblem::getRowLowerBound(int row) {
    return glp_get_row_lb(problem, row);
}

double GLPKProblem::getRowUpperBound(int row) {
    return glp_get_row_ub(problem, row);
}

void GLPKProblem::solve() {
	// Configura a base inicial
//	glp_std_basis(problem);
//	glp_adv_basis(problem, 0);
//	glp_cpx_basis(problem);
	
    // Call Simplex
	glp_simplex(problem, &parm);
}

void GLPKProblem::getSolution() {
    // Obtem o resultado
    objective = glp_get_obj_val(problem);
    
	solution.clear();
	solution.push_back(0);
    for (int i=1; i <= columns; i++) {
		solution.push_back(glp_get_col_prim(problem, i));
    }
}

void GLPKProblem::printSolution() {
    // Imprime o resultado
    printf("z = %g\n", objective);
    for (int i=1; i <= columns; i++) {
        printf("x[%d] = %g\n", i, solution[i]);
    }
}

void GLPKProblem::sparceMatrixVisualization(double **matrix, int rows, int columns) {
    ofstream myFile ("/Users/leoformaggio/Desktop/matrix.pbm");
    
    int entry;
    
    if (myFile.is_open()) {
        
        myFile << "P1" << endl;
        myFile << "# PBM example" << endl;
        myFile << columns << " " << rows << endl;
        
        for (unsigned int i = 1; i <= rows; i++) {
            for (unsigned int j = 1; j <= columns; j++) {
                
                if (matrix[i][j] == 0.0) {
                    entry = 0;
                } else {
                    entry= 1;
                }
                
                myFile << entry << " ";
            }
            myFile << endl;
        }
        myFile.close();
    } else {
        cout << "Unable to open file\n";
    }
}