//
//  GLPKProblem.h
//  TCC
//
//  Created by Leonardo Formaggio on 9/18/11.
//  Copyright 2011 Fingertips. All rights reserved.
//

#ifndef TCC_GLPKProblem_h
#define TCC_GLPKProblem_h

#include <iostream>
#include <vector>
#include <map>
#include <glpk.h>

class GLPKProblem {
    // instance variables
protected:
    glp_smcp parm;
    glp_prob *problem;
public:
    // Constraint matrix dimension
    int rows;
    int columns;
    
    // objective funcion value
    double objective;
    
    // problem solution
    std::vector<double> solution;
    
	// number of non-zeros in solution
    int nonZeros;
    
    // Constructors
    GLPKProblem();
    GLPKProblem(int _rows, int _columns);
    // Destructor
    virtual ~GLPKProblem();
    
    // GLPK Problem Methods
    void setProblemName(std::string name);
    void setOptimizationDirection(int direction);
    void loadMatrix(double **matrix);
    void setMatrixEntry(int row, int column, double value);
    void setObjectiveFunction(const double *coef);
    void setColumnBounds(int _column, int type, double lowerBound, double upperBound);
	void setColumnBounds(int start, int end, int type, double lowerBound, double upperBound);
    void setRowBounds(int _row, int type, double lowerBound, double upperBound);
    void setRowBounds(int start, int end, int type, double lowerBounds, double upperBounds);
    void setRowBounds(int start, int end, int type, double *lowerBounds, double *upperBounds);
    
    int getRowStatus(int row);
    double getRowLowerBound(int row);
    double getRowUpperBound(int row);
    
    void solve();
    void getSolution();
    void printSolution();
	void sparceMatrixVisualization(double **matrix, int rows, int columns);
};


#endif
