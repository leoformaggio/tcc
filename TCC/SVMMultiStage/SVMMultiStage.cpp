//
//  SVMMultiStage.cpp
//  TCC
//
//  Created by Leonardo Formaggio on 9/20/11.
//  Copyright 2011 Fingertips. All rights reserved.
//

#include "SVMMultiStage.h"
#include "float.h"
#include "math.h"

using namespace std;

#pragma mark - Constructors / Destructors

SVMMultiStage::SVMMultiStage() : GLPKProblem() {
    
}

SVMMultiStage::SVMMultiStage(int _rows, int _columns) : GLPKProblem(2*_rows + 1, 2*_columns + _rows + 2) {
	this->setOriginalDataSize(_rows, _columns);
	this->buildObjectiveFunction();
	this->setOptimizationDirection(GLP_MAX);
	this->setColumnBounds(1, columns, GLP_LO, 0.0, 0.0);
	this->setColumnBounds(columns-1, GLP_FR, 0.0, 0.0);
	this->setRowBounds(1, GLP_FX, 1.0, 0.0);
	this->setRowBounds(2, dataRows+1, GLP_FX, 0.0, 0.0);
	this->setRowBounds(dataRows+2, rows, GLP_LO, 0.0, 0.0);
	this->slackness = 0.2;
}

SVMMultiStage::~SVMMultiStage() {
	
}

#pragma mark - Multi Stage SVM Method


void SVMMultiStage::loadData(double **data, double *labels) {
	double **retMatrix;
    
    // allocate memory for the GLPK matrix
    retMatrix = (double **)malloc((rows+1) * sizeof(double *));
    for (int i=1; i <= rows; i++) {
        retMatrix[i] = (double *)malloc((columns+1) * sizeof(double));
    }
    
    // set 0.0 for all entries
    for (int i=1; i <= rows; i++) {
        for (int j=1; j <= columns; j++) {
            retMatrix[i][j] = 0.0;
        }
    }
    
    // sum w^{+} + w^{-} = 1.0
    for (int j=1; j <= 2*dataColumns; j++) {
        retMatrix[1][j] = 1.0;
    }
    
    // pass data to retMatrix
    for (int i=2; i <= (dataRows + 1); i++) {
        int label = (int)labels[i-1];
        for (int j=1; j <= dataColumns; j++) {
            // w^{+}
            retMatrix[i][j] = label * data[i-1][j];
            // w^{-}
            retMatrix[i][j+dataColumns] = - label * data[i-1][j];
        }
    }
    
    for (int i=2; i <= (dataRows+1); i++) {
        // r^{0} and r^{1} of hiperplane equation
        retMatrix[i][i+2*dataColumns-1] = -1.0;
        
        // r^{0} -\zeta >= 0
        // r^{1} -\zeta >= 0
        retMatrix[i+dataRows][i+2*dataColumns-1] = 1.0;
    }
    
    // \gamma
    for (int i = 2; i <= (dataRows+1); i++) {
        double label = labels[i-1];
        retMatrix[i][columns-1] = -label;
    }
    
    // r - z >= 0
    for (int i=(1+dataRows+1); i <= rows; i++) {
        retMatrix[i][columns] = -1.0;
    }
	
	this->loadMatrix(retMatrix);
    
	free(retMatrix);
}

void SVMMultiStage::buildObjectiveFunction() {
	double *coef = (double *)malloc((columns+1)*sizeof(double));
    for (int i=1; i <= columns; i++) {
        coef[i] = 0.0;
    }
    coef[columns] = 1.0;
	
	this->setObjectiveFunction(coef);
	
	free(coef);
}

void SVMMultiStage::run() {
	stages = 0;
	parm.meth = GLP_DUAL;
	
	while (this->hasZetaUnfixed()) {
		
		oldW = w;
		
		map<int, double>::iterator it;
		for (it = fixedZetas.begin(); it != fixedZetas.end(); it++) {
			if (fixedZetasFlags[it->first] == false) {
				// MELHORIA SUGERIDA POR PAULO
				int varIndex = it->first - (dataRows + 1) + 2*dataColumns;
				this->setRowBounds(it->first, GLP_LO, -INT_MAX, 0.0);
				this->setColumnBounds(varIndex, GLP_LO, (1 - slackness)*it->second, 0.0);
				
				fixedZetasFlags[it->first] = true;
			}
		}
		
		this->solve();
		this->getSolution();
		this->fixZetaVariables();
		this->getHyperplane();
		
		for (it = fixedZetas.begin(); it != fixedZetas.end(); it++) {
			if (fixedZetasFlags.count(it->first) == 0) {
				fixedZetasFlags[it->first] = false;
			}
		}
		stages++;
	}
}

void SVMMultiStage::fixZetaVariables() {
	
	for (int i=(dataRows + 2); i <= rows; i++) {
		
		int rowStatus = this->getRowStatus(i);
		
		if (rowStatus == GLP_NL) {
            double bound = this->getRowLowerBound(i);
            if (bound == 0.0) {
                bound = objective;
            }
            fixedZetas[i] = bound;
        }
    }
}

#pragma mark - Auxiliary Methods

void SVMMultiStage::setRadiusVariablesRange(int start, int end) {
    radiusVariablesStart = start;
    radiusVariablesEnd = end;
}

void SVMMultiStage::setOriginalDataSize(int rows, int columns) {
    dataRows = rows;
    dataColumns = columns;
    
    this->setRadiusVariablesRange(2*dataColumns+1, 2*dataColumns+dataRows);
}

void SVMMultiStage::printSolution() {
    printf("Fixed zetas: %d\n", (int)fixedZetas.size());
    
    // Imprime o resultado
//    printf("z = %g\n", objective);
    for (int i=1; i < w.size(); i++) {
        printf("w[%d] = %g\n", i, w[i]);
    }
}

void SVMMultiStage::getHyperplane() {
	w.clear();
    w.push_back(0);
    
    nonZeros = 0;
    
    for (int i = 1; i <= dataColumns; i++) {
        w.push_back(solution[i] - solution[i+dataColumns]);
        if (fabs(w[i]) > DBL_EPSILON) {
            nonZeros++;
        }
    }
    
    gamma = solution[columns-1];
}

double SVMMultiStage::vectorsDiff(vector<double> sol1, vector<double> sol2) {
	double distance = 0.0;
	
	if (sol1.size() == 0 || sol2.size() == 0) {
		return INT_MAX;
	}
	
	for (int i=1; i < sol1.size(); i++) {
		double diff = fabs(sol2[i] - sol1[i]);
		if (diff > distance) {
			distance = diff;
		}
	}
	
	return distance;
}

bool SVMMultiStage::normalChanged() {
	bool boolRet;
	
	boolRet = !(vectorsDiff(w, oldW) < DBL_EPSILON);
	
	return boolRet;
}

bool SVMMultiStage::hasZetaUnfixed() {
	unsigned int count = 0;
	map<int, bool>::iterator it;
	for (it = fixedZetasFlags.begin(); it != fixedZetasFlags.end(); it++) {
		if (it->second == true) {
			count++;
		}
	}
	return count < dataRows;
}