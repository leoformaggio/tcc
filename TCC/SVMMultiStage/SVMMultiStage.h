//
//  SVMMultiStage.h
//  TCC
//
//  Created by Leonardo Formaggio on 9/20/11.
//  Copyright 2011 Fingertips. All rights reserved.
//

#ifndef TCC_SVMMultiStage_h
#define TCC_SVMMultiStage_h

#include "GLPKProblem.h"

class SVMMultiStage : public GLPKProblem {
private:
    void setRadiusVariablesRange(int start, int end);
protected:
    int radiusVariablesStart;
    int radiusVariablesEnd;
public:
    std::vector<double> w;
    double gamma;
	
	std::vector<double> oldW;
	
    std::map<int, double> fixedZetas; // index and radius of fixed zetas
	std::map<int, bool> fixedZetasFlags; // flags used to know which variable is fixed
	
	// Slackness for each stage
	double slackness;
	
	// number of stages
	int stages;
    
    // Constructor
    SVMMultiStage();
    SVMMultiStage(int _rows, int _columns);
    // Destructor
    virtual ~SVMMultiStage();
    
    // Original data dimension
    int dataRows;
    int dataColumns;
	
	void loadData(double **data, double *labels);
	void buildObjectiveFunction();
    
	void run();
	void getHyperplane();
	
    void setOriginalDataSize(int rows, int columns);
    void fixZetaVariables();
    
    void printSolution();
	
	double vectorsDiff(std::vector<double> sol1, std::vector<double> sol2);
	bool normalChanged();
	bool hasZetaUnfixed();
};

#endif