//
//  CSVM.h
//  TCC
//
//  Created by Leonardo Formaggio on 5/27/13.
//  Copyright (c) 2013 Fingertips. All rights reserved.
//

#ifndef __TCC__CSVM__
#define __TCC__CSVM__

#include <iostream>
#include "GLPKProblem.h"

class CSVM : public GLPKProblem {
private:
	std::vector<double> xca; // -1
	std::vector<double> xcb; // +1
	
	void determineCenters(double **data, double *labels);
protected:
public:
	std::vector<double> w;
	double gamma;
	
    // Constructor
    CSVM();
    CSVM(int _rows, int _columns);
    // Destructor
    virtual ~CSVM();
    
	// Original data dimension
    int dataRows;
    int dataColumns;
    
    void setOriginalDataSize(int rows, int columns);
    
	void loadData(double **data, double *labels);
	void buildObjectiveFunction();
	void run();
    void getHyperplane();
	
	
	void printSolution();
};

#endif
