//
//  CSVM.cpp
//  TCC
//
//  Created by Leonardo Formaggio on 5/27/13.
//  Copyright (c) 2013 Fingertips. All rights reserved.
//

#include "CSVM.h"
#include "math.h"
#include <float.h>
#include <numeric>

#pragma mark - Constructors / Destructors

CSVM::CSVM() : GLPKProblem() {
    
}

CSVM::CSVM(int _rows, int _columns) : GLPKProblem(_rows + 1, 2*_columns + 2) {
	this->setOriginalDataSize(_rows, _columns);
	this->buildObjectiveFunction();
    this->setOptimizationDirection(GLP_MIN);
	this->setColumnBounds(1, columns - 2, GLP_LO, 0.0, 0.0);
    this->setColumnBounds(columns - 1, GLP_FR, 0.0, 0.0);
	this->setColumnBounds(columns, GLP_LO, 0.0, 0.0);
    this->setRowBounds(1, rows - 1, GLP_LO, 0.0, 0.0);
	this->setRowBounds(rows, GLP_FX, 1.0, 0.0);
}

CSVM::~CSVM() {
	
}

#pragma mark - Private

void CSVM::determineCenters(double **data, double *labels) {
	xca = std::vector<double>();
	xcb = std::vector<double>();
	
	for (int j = 1; j <= dataColumns; j++) {
		std::vector<double> xa = std::vector<double>();
		std::vector<double> xb = std::vector<double>();
		
		for (int i = 1; i <= dataRows; i++) {
			if (labels[i] == -1) {
				xa.push_back(data[i][j]);
			} else {
				xb.push_back(data[i][j]);
			}
		}
		
		xca.push_back(std::accumulate(xa.begin(), xa.end(), 0.0) / xa.size());
		xcb.push_back(std::accumulate(xb.begin(), xb.end(), 0.0) / xb.size());
	}	
}

#pragma mark - Classic SVM Methods

void CSVM::loadData(double **data, double *labels) {
	double **retMatrix = NULL;
	
	this->determineCenters(data, labels);
    
    // allocate memory for the GLPK matrix
    retMatrix = (double **)malloc((rows+1) * sizeof(double *));
    for (int i=1; i <= rows; i++) {
        retMatrix[i] = (double *)malloc((columns+1) * sizeof(double));
    }
    
    // set 0.0 for all entries
    for (int i=1; i <= rows; i++) {
        for (int j=1; j <= columns; j++) {
            retMatrix[i][j] = 0.0;
        }
    }
    
    // pass data to retMatrix
    for (int i=1; i <= dataRows; i++) {
        double label = labels[i];
        for (int j=1; j <= dataColumns; j++) {
			// w^+
            retMatrix[i][j] = label * data[i][j];
            // w^-
            retMatrix[i][j+dataColumns] = - label * data[i][j];
        }
    }
	
	for (int j=1; j <= dataColumns; j++) {
		// w^{+}
		retMatrix[rows][j] = xcb[j-1] - xca[j-1];
		// w^{-}
		retMatrix[rows][j+dataColumns] = xca[j-1] - xcb[j-1];
	}
    
    for (int i = 1; i <= dataRows; i++) {
        double label = labels[i];
		// gamma
        retMatrix[i][columns-1] = -label;
		// csi
		retMatrix[i][columns] = -1;
    }
    
	this->loadMatrix(retMatrix);
	
	free(retMatrix);
}

void CSVM::buildObjectiveFunction() {
	double *coef = (double *)malloc((columns+1)*sizeof(double));
    for (int i=1; i < columns; i++) {
        coef[i] = 1.0;
    }
    coef[columns] = 0.0;
	coef[columns-1] = 0.0;
	
	this->setObjectiveFunction(coef);
	
	free(coef);
}

#pragma mark - Auxiliary Methods

void CSVM::setOriginalDataSize(int rows, int columns) {
    dataRows = rows;
    dataColumns = columns;
}

void CSVM::printSolution() {
    // Imprime o resultado
	//    printf("z = %g\n", objective);
    for (int i=1; i < w.size(); i++) {
        printf("w[%d] = %g\n", i, w[i]);
    }
}

void CSVM::run() {
	this->solve();
	this->getSolution();
	this->getHyperplane();
}

void CSVM::getHyperplane() {
    w.clear();
	w.push_back(0);
	
    nonZeros = 0;
    
    for (int i=1; i<=dataColumns; i++) {
		w.push_back(solution[i] - solution[i+dataColumns]);
		
        if (fabs(w[i]) > DBL_EPSILON) {
            nonZeros++;
        }
    }
	gamma = solution[columns-1];
}
