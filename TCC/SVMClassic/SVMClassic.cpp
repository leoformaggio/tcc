//
//  SVMClassic.cpp
//  TCC
//
//  Created by Leonardo Formaggio on 9/27/11.
//  Copyright 2011 Fingertips. All rights reserved.
//

#include "SVMClassic.h"
#include "math.h"
#include <float.h>

#pragma mark - Constructors / Destructors

SVMClassic::SVMClassic() : GLPKProblem() {
    
}

SVMClassic::SVMClassic(int _rows, int _columns) : GLPKProblem(_rows, 2*_columns + 1) {
	this->setOriginalDataSize(_rows, _columns);
	this->buildObjectiveFunction();
    this->setOptimizationDirection(GLP_MIN);
	this->setColumnBounds(1, columns-1, GLP_LO, 0.0, 0.0);
    this->setColumnBounds(columns, GLP_FR, 0.0, 0.0);
    this->setRowBounds(1, rows, GLP_LO, 1.0, 0.0);
}

SVMClassic::~SVMClassic() {
	
}

#pragma mark - Classic SVM Methods

void SVMClassic::loadData(double **data, double *labels) {
	double **retMatrix;
    
    // allocate memory for the GLPK matrix
    retMatrix = (double **)malloc((rows+1) * sizeof(double *));
    for (int i=1; i <= rows; i++) {
        retMatrix[i] = (double *)malloc((columns+1) * sizeof(double));
    }
    
    // set 0.0 for all entries
    for (int i=1; i <= rows; i++) {
        for (int j=1; j <= columns; j++) {
            retMatrix[i][j] = 0.0;
        }
    }
    
    // pass data to retMatrix
    for (int i=1; i <= dataRows; i++) {
        double label = labels[i];
        for (int j=1; j <= dataColumns; j++) {
            // w^{+}
            retMatrix[i][j] = label * data[i][j];
            // w^{-}
            retMatrix[i][j+dataColumns] = - label * data[i][j];
        }
    }
    
    // gamma
    for (int i = 1; i <= rows; i++) {
        double label = labels[i];
        retMatrix[i][columns] = -label;
    }
    
	this->loadMatrix(retMatrix);
	
	free(retMatrix);
}

void SVMClassic::buildObjectiveFunction() {
	double *coef = (double *)malloc((columns+1)*sizeof(double));
    for (int i=1; i < columns; i++) {
        coef[i] = 1.0;
    }
    coef[columns] = 0.0;
	
	this->setObjectiveFunction(coef);
	
	free(coef);
}

#pragma mark - Auxiliary Methods

void SVMClassic::setOriginalDataSize(int rows, int columns) {
    dataRows = rows;
    dataColumns = columns;
}

void SVMClassic::printSolution() {
    // Imprime o resultado
//    printf("z = %g\n", objective);
    for (int i=1; i < w.size(); i++) {
        printf("w[%d] = %g\n", i, w[i]);
    }
}

void SVMClassic::run() {
	this->solve();
	this->getSolution();
	this->getHyperplane();
}

void SVMClassic::getHyperplane() {
    w.clear();
	w.push_back(0);
	
    nonZeros = 0;
    
    for (int i=1; i<=dataColumns; i++) {
		w.push_back(solution[i] - solution[i+dataColumns]);
		
        if (fabs(w[i]) > DBL_EPSILON) {
            nonZeros++;
        }
    }
	gamma = solution[columns];
}