//
//  SVMClassic.h
//  TCC
//
//  Created by Leonardo Formaggio on 9/27/11.
//  Copyright 2011 Fingertips. All rights reserved.
//

#ifndef TCC_SVMClassic_h
#define TCC_SVMClassic_h

#include "GLPKProblem.h"

class SVMClassic : public GLPKProblem {
private:
protected:
public:
	std::vector<double> w;
	double gamma;
    
    // Constructor
    SVMClassic();
    SVMClassic(int _rows, int _columns);
    // Destructor
    virtual ~SVMClassic();
    
    // Original data dimension
    int dataRows;
    int dataColumns;
    
    
    void setOriginalDataSize(int rows, int columns);
    
	void loadData(double **data, double *labels);
	void buildObjectiveFunction();
	void run();
    void getHyperplane();
	
	
	void printSolution();
};

#endif
