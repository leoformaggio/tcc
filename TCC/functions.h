//
//  functions.h
//  TCC
//
//  Created by Leonardo Formaggio on 5/24/11.
//  Copyright 2011 Fingertips. All rights reserved.
//

#include <vector>
#include <map>
#include <stdlib.h>

using namespace std;

namespace functions {
	double **transposed_matrix(double **matrix, int rows, int columns);
	double normaUm(double **matrix, int rows, int columns);
	double normaInfinito(double **matrix, int rows, int columns);
	void sparceMatrixVisualization(double **matrix, int rows, int columns);
	double compareSolutions(double *solution1, double *solution2, int length);
	
	void compareClassicAndMultiStageSVM(std::string fileName, double slackness);
	void runMultiStageSVM(std::string fileName, std::vector<double> slackness);
	std::map<float, double> cross_validation(std::string fileName, std::vector<float> slackness, int numberOfFolds);
	double testMultistage(std::string fileName, double slackness);
	bool test_point(const std::vector<double> w, const double gamma, const std::vector<double> x, int label);
	void generateSampleData(int classSize);
}
