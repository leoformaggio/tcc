//
//  Reader.h
//  Reader
//
//  Created by Leonardo Formaggio on 8/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <string>

class Reader {
    public:
    // instance variables
    unsigned int rows;
    unsigned int cols;

    double **matrix;
    double *vector;
    
    // constructors and destructors
    Reader();
    ~Reader();
    
    // member functions
    void getDataDimensionFromFile(std::string fileName);
    void readDataFromFile(std::string fileName);
};
