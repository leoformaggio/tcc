//
//  Reader.cpp
//  Reader
//
//  Created by Leonardo Formaggio on 8/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "Reader.h"
#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

Reader::Reader() {
    // ...
}

Reader::~Reader() {
    // ...
}

void Reader::getDataDimensionFromFile(string fileName) {
    ifstream myFile (fileName.c_str());
    if (myFile.is_open()) {
        
        rows = 0;
        cols = 0;
        
        string line;
        while (getline(myFile,line)) {
            stringstream s(line);
            rows++;
            string token;
            while (getline(s,token,' ')) {
                cols++;
            }
        }
        cols /= rows;
        
        myFile.close();
    } else {
        cout << "Unable to open file\n";
    }
}

void Reader::readDataFromFile(string fileName) {
    
    this->getDataDimensionFromFile(fileName);
    
    ifstream myFile (fileName.c_str());
    if (myFile.is_open()) {
            
        if (cols == 1 || rows == 1) {
            unsigned int length = max(cols, rows);
            vector = (double *)malloc((length+1) * sizeof(double));
            
            for (unsigned int i = 1; i <= length; i++) {
                myFile >> vector[i];
            }
        } else {
            matrix = (double **)malloc((rows+1) * sizeof(double *));
            for (int i=1; i <= (rows+1); i++) {
                matrix[i] = (double *)malloc((cols+1) * sizeof(double));
            }
            
            for (unsigned int i = 1; i <= rows; i++) {
                for (unsigned int j = 1; j <= cols; j++) {
                    myFile >> matrix[i][j];
                }
            }
        }
        
        myFile.close();
    } else {
        cout << "Unable to open file\n";
    }
}
