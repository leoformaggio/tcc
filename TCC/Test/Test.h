//
//  Test.h
//  TCC
//
//  Created by Leonardo Formaggio on 5/29/13.
//  Copyright (c) 2013 Fingertips. All rights reserved.
//

#ifndef __TCC__Test__
#define __TCC__Test__

#include <iostream>
#include "functions.h"
#include "Reader.h"
#include "SVMMultiStage.h"

template <class T>
class Test {
protected:
	double _slack;
	std::string _file;
public:
	Test(std::string file);
	Test(std::string file, double slack);
	virtual ~Test();
	
	double slack();
	
	double run();
};

template <class T>
Test<T>::Test(std::string file) {
	_file = file;
}

template <class T>
Test<T>::Test(std::string file, double slack) {
	_file = file;
	_slack = slack;
}

template <class T>
Test<T>::~Test() {
	
}

template <class T>
double Test<T>::slack() {
	return _slack;
}

template <class T>
double Test<T>::run() {
	int training_size;
    int dimension;
	int test_size;
	int error_counter;
	double hits;
	
	Reader *dataReader = new Reader;
    dataReader->readDataFromFile(_file + ".dat");
    training_size = dataReader->cols;
    dimension = dataReader->rows;
    double **data = functions::transposed_matrix(dataReader->matrix, dataReader->rows, dataReader->cols);
    delete dataReader;
    
    Reader *labelReader = new Reader;
    labelReader->readDataFromFile(_file + ".cls");
    double *labels = labelReader->vector;
    delete labelReader;
	
	Reader *testDataReader = new Reader;
    testDataReader->readDataFromFile(_file + "_test.dat");
    test_size = testDataReader->cols;
    double **testData = functions::transposed_matrix(testDataReader->matrix, testDataReader->rows, testDataReader->cols);
    delete testDataReader;
    
    Reader *testLabelReader = new Reader;
    testLabelReader->readDataFromFile(_file + "_test.cls");
    double *testLabels = testLabelReader->vector;
    delete testLabelReader;
	
	T *m = new T(training_size, dimension);
	m->loadData(data, labels);
	if (typeid(T) == typeid(SVMMultiStage)) {
		dynamic_cast<SVMMultiStage *>(m)->slackness = _slack;
	}
	m->run();
	
	// Test the solution for the excluded points
	error_counter = 0;
	for (int i = 1; i <= test_size; i++) {
		vector<double> point = vector<double>();
		point.push_back(0);
		for (int j=1; j <= dimension; j++) {
			point.push_back(testData[i][j]);
		}
		
		if (functions::test_point(m->w, m->gamma, point, testLabels[i]) == false) {
			error_counter++;
		}
	}
	
	hits = 100 * (1 - (double)error_counter/test_size);
	
	if (hits > 0) {
		delete m;
		free(data);
		free(labels);
		free(testData);
		free(testLabels);
	}
	
	return hits;
}


#endif
