//
//  functions.c
//  TCC
//
//  Created by Leonardo Formaggio on 5/24/11.
//  Copyright 2011 Fingertips. All rights reserved.
//

#include "functions.h"

#include <fstream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <glpk.h>

#include "math.h"

#include "Reader.h"
#include "SVMMultiStage.h"
#include "SVMClassic.h"
#include "CSVM.h"
#include "MSVM.h"

using namespace std;

double** functions::transposed_matrix(double **matrix, int rows, int columns) {
    double **retMatrix;
    retMatrix = (double **)malloc((columns+1) * sizeof(double *));
    for (int j=1; j <= columns; j++) {
        retMatrix[j] = (double *)malloc((rows+1) * sizeof(double));
    }
    
    for (int i=1; i <= rows; i++) {
        for (int j=1; j <= columns; j++) {
            retMatrix[j][i] = matrix[i][j];
        }
    }
    
    return retMatrix;
}

double normaUm(double **matrix, int rows, int columns) {
    double norm = 0;
    double diff = 0;
    
    double distances[rows+1][rows+1];
    
    for (int k=1; k<=rows; k++) {
        for (int i=1; i <= rows; i++) {
            for (int j=1; j<=columns; j++) {
                diff += fabs(matrix[k][j] - matrix[i][j]);
            }
            if (diff > norm) {
                norm = diff;
            }
            
            distances[k][i] = diff;
            
            diff = 0;
        }
    }
    
    
    for (int i=1; i <= rows; i++) {
        for (int j=1; j<=rows; j++) {
            printf("%lf ", distances[i][j]);
        }
        printf("\n");
    }
    
    return norm;
}

double normaInfinito(double **matrix, int rows, int columns) {
    double norm = 0;
    double diff = 0;
    
    double distances[rows+1][rows+1];
    
    for (int k=1; k<=rows; k++) {
        for (int i=1; i <= rows; i++) {
            for (int j=1; j<=columns; j++) {
                diff = fabs(matrix[k][j] - matrix[i][j]);
                if (diff > norm) {
                    norm = diff;
                }
            }
            
            distances[k][i] = norm;
            
            diff = 0;
            norm = 0;
        }
    }
    
    
    for (int i=1; i <= rows; i++) {
        for (int j=1; j<=rows; j++) {
            printf("%lf ", distances[i][j]);
        }
        printf("\n");
    }
    
    return norm;
}

void sparceMatrixVisualization(double **matrix, int rows, int columns) {
    ofstream myFile ("/Users/leoformaggio/Desktop/matrix.pbm");
    
    int entry;
    
    if (myFile.is_open()) {
        
        myFile << "P1" << endl;
        myFile << "# PBM example" << endl;
        myFile << columns << " " << rows << endl;
        
        for (unsigned int i = 1; i <= rows; i++) {
            for (unsigned int j = 1; j <= columns; j++) {
                
                if (matrix[i][j] == 0.0) {
                    entry = 0;
                } else {
                    entry= 1;
                }
                
                myFile << entry << " ";
            }
            myFile << endl;
        }
        myFile.close();
    } else {
        cout << "Unable to open file\n";
    }
}

double compareSolutions(vector<double> solution1, vector<double> solution2) {
    double ret = 0.0;
    double norm1 = 0.0;
    double norm2 = 0.0;
	int length = (int)solution1.size();
    
//    for (int i = 1; i < length; i++) {
//        if (solution1[i] != 0.0) {
//            printf("Solution 1: %d\n", i);
//        }
//    }
//    
//    for (int i = 1; i < length; i++) {
//        if (solution2[i] != 0.0) {
//            printf("Solution 2: %d\n", i);
//        }
//    }
    
    for (int i = 1; i < length; i++) {
        if (fabs(solution1[i]) > norm1) {
            norm1 = fabs(solution1[i]);
        }
        if (fabs(solution2[i]) > norm2) {
            norm2 = fabs(solution2[i]);
        }
    }
    
    for (int i = 1; i < length; i++) {
        solution1[i] /= norm1;
        solution2[i] /= norm2;
    }
    
    for (int i = 1; i < length; i++) {
        if (fabs(solution2[i] - solution1[i]) > ret) {
            ret = fabs(solution2[i] - solution1[i]);
        }
    }
    
    return ret;
}

void compareClassicAndMultiStageSVM(string fileName, double slackness) {
    int linhas;
    int colunas;
    
    Reader *dataReader = new Reader;
    dataReader->readDataFromFile(fileName + ".dat");
    linhas = dataReader->cols;
    colunas = dataReader->rows;
    double **data = functions::transposed_matrix(dataReader->matrix, dataReader->rows, dataReader->cols);
    delete dataReader;
    
    Reader *labelReader = new Reader;
    labelReader->readDataFromFile(fileName + ".cls");
    double *labels = labelReader->vector;
    delete labelReader;
    
    SVMClassic *classic = new SVMClassic(linhas, colunas);
	classic->loadData(data, labels);
    classic->run();
	classic->printSolution();
	
	SVMMultiStage *multi = new SVMMultiStage(linhas, colunas);
	multi->loadData(data, labels);
	multi->slackness = slackness;
	multi->run();
	multi->printSolution();
	
    // Comparar w aqui
//    double wDiff = compareSolutions(classic->w, multi->w);
	
	printf("Classic non-zeros: %d\n", classic->nonZeros);
	printf("Multi Stage non-zeros: %d\n", multi->nonZeros);
//    printf("|w2 - w1| = %g\n", wDiff);
    printf("Number of stages: %d\n", multi->stages);
    
    delete classic;
    delete multi;
    
    free(data);
    free(labels);
}

void runMultiStageSVM(string fileName, vector<double> slackness) {
    int linhas;
    int colunas;
	vector<int> nonZeros;
	vector<int> stages;
    
    Reader *dataReader = new Reader;
    dataReader->readDataFromFile(fileName + ".dat");
    linhas = dataReader->cols;
    colunas = dataReader->rows;
    double **data = functions::transposed_matrix(dataReader->matrix, dataReader->rows, dataReader->cols);
    delete dataReader;
    
    Reader *labelReader = new Reader;
    labelReader->readDataFromFile(fileName + ".cls");
    double *labels = labelReader->vector;
    delete labelReader;
	
	for (int i=0; i < slackness.size(); i++) {
		SVMMultiStage *multi = new SVMMultiStage(linhas, colunas);
		multi->loadData(data, labels);
		multi->slackness = slackness[i];
		multi->run();
		
		nonZeros.push_back(multi->nonZeros);
		stages.push_back(multi->stages);
		
		delete multi;
	}
	
	printf("******* NON-ZEROS ********\n");
	for (int idx=0; idx<slackness.size(); idx++) {
		printf("%d\n", nonZeros[idx]);
	}
	
	printf("******** STAGES **********\n");
	for (int idx=0; idx<slackness.size(); idx++) {
		printf("%d\n", stages[idx]);
	}
	
    free(data);
    free(labels);
}

map<float, double> functions::cross_validation(string fileName, vector<float> slackness, int numberOfFolds) {
    int linhas;
    int colunas;
	
	map<float, double> hits;
//	for (int i = 0; i < slackness.size(); i++) {
//		hits.push_back(0);
//	}
	
    int foldMeanSize;
    vector<vector<int> > folds;
    vector<int> labelsCopy;
    vector<int> firstClassIndex;
    vector<int> secondClassIndex;
    vector<int> foldsFirstClassCounter = vector<int>(numberOfFolds); // -1 labels count
    vector<int> foldsSecondClassCounter = vector<int>(numberOfFolds); // 1 labels count
    int firstClassTotal = 0;
    int secondClassTotal = 0;
    
    Reader *dataReader = new Reader;
    dataReader->readDataFromFile(fileName + ".dat");
    linhas = dataReader->cols;
    colunas = dataReader->rows;
    double **data = functions::transposed_matrix(dataReader->matrix, dataReader->rows, dataReader->cols);
    delete dataReader;
    
    Reader *labelReader = new Reader;
    labelReader->readDataFromFile(fileName + ".cls");
    double *labels = labelReader->vector;
    labelsCopy.assign(labels + 1, labels + linhas + 1);
    delete labelReader;
    
    for (int i = 1; i <= labelsCopy.size(); i++) {
        if (labelsCopy[i-1] == -1) {
            firstClassIndex.push_back(i);
        } else if (labelsCopy[i-1] == 1) {
            secondClassIndex.push_back(i);
        }
    }
	
	firstClassTotal = (int)firstClassIndex.size();
	secondClassTotal = (int)secondClassIndex.size();
    
    // Building k-Folds
    folds = vector<vector<int> >(numberOfFolds);
    foldMeanSize = (int)labelsCopy.size()/numberOfFolds;
    
    for (int i=0; i < numberOfFolds; i++) {
		
		while (folds[i].size() < foldMeanSize) {
            // Get random index from labelsIndexes and put on folds
            
            if (foldsFirstClassCounter[i] < firstClassTotal/numberOfFolds) {
                int size = (int)firstClassIndex.size();
                int randomIndex = size > 1 ? (arc4random() % (size-1)) : 0;
                
                folds[i].push_back(firstClassIndex[randomIndex]);
                firstClassIndex.erase(firstClassIndex.begin()+randomIndex);
                foldsFirstClassCounter[i] += 1;
            }
            
            if (foldsSecondClassCounter[i] < secondClassTotal/numberOfFolds) {
                int size = (int)secondClassIndex.size();
                int randomIndex = size > 1 ? (arc4random() % (size-1)) : 0;
                
                folds[i].push_back(secondClassIndex[randomIndex]);
                secondClassIndex.erase(secondClassIndex.begin()+randomIndex);
                foldsSecondClassCounter[i] += 1;
            }
        }
    }
    
    for (int i=0; i < firstClassIndex.size(); i++) {
        folds[i].push_back(firstClassIndex[i]);
    }
    
    for (int i=0; i < secondClassIndex.size(); i++) {
        folds[i].push_back(secondClassIndex[i]);
    }
    // End Building k-Folds
    
    // Building a new data set by excluding a fold, and solving the problem related to it
    
    int errorCounter;
    
    for (int k=0; k < numberOfFolds; k++) {
        int newLinhas = linhas - (int)folds[k].size();
        int newColunas = colunas;
        
        vector<int> outIndexes;
        for (int i=0; (i < numberOfFolds); i++) {
            if (i != k) {
                for (int j=0; j < folds[i].size(); j++) {
                    outIndexes.push_back(folds[i][j]);
                }
            }
        }
        
        // Build new data
        double **newData = (double **)malloc((newLinhas+1) * sizeof(double *));
        for (int j=1; j <= newLinhas; j++) {
            newData[j] = (double *)malloc((newColunas+1) * sizeof(double));
        }
        for (int i=1; i <= newLinhas; i++) {
            for (int j=1; j <= newColunas; j++) {
                newData[i][j] = data[outIndexes[i-1]][j];
            }
        }
        
        // Build new labels
        double *newLabels = (double *)malloc((newLinhas + 1) * sizeof(double));
        for (int i=1; i <= newLinhas; i++) {
            newLabels[i] = labels[outIndexes[i-1]];
        }
		
		vector<float>::iterator it;
		for (it = slackness.begin(); it != slackness.end(); it++) {
			
			SVMMultiStage *multi = new SVMMultiStage(newLinhas, newColunas);
			multi->loadData(newData, newLabels);
			multi->slackness = *it;
			multi->run();
			
			// Test the solution for the excluded points
			errorCounter = 0;
			vector<int>::iterator index;
			vector<double> point;
			for (index = folds[k].begin(); index != folds[k].end(); index++) {
				point.clear();
				point.push_back(0);
				for (int j=1; j <= newColunas; j++) {
					point.push_back(data[*index][j]);
				}
				
				if (functions::test_point(multi->w, multi->gamma, point, labels[*index]) == false) {
					errorCounter++;
				}
			}
			
			delete multi;
			
			hits[*it] += errorCounter;
		}
        
        free(newData);
        free(newLabels);
    }
    
    // End building a new data set
	
	vector<float>::iterator it;
	for (it = slackness.begin(); it != slackness.end(); it++) {
		hits[*it] = 100 * (1 - hits[*it] / linhas);
	}
    
    free(data);
    free(labels);
	
	return hits;
}

double testMultistage(string fileName, double slackness) {
	int linhas;
    int colunas;
	int testLinhas;
	int testColunas;
	int errorCounter;
	double doubleRet;
	
	Reader *dataReader = new Reader;
    dataReader->readDataFromFile(fileName + ".dat");
    linhas = dataReader->cols;
    colunas = dataReader->rows;
    double **data = functions::transposed_matrix(dataReader->matrix, dataReader->rows, dataReader->cols);
    delete dataReader;
    
    Reader *labelReader = new Reader;
    labelReader->readDataFromFile(fileName + ".cls");
    double *labels = labelReader->vector;
    delete labelReader;
	
	Reader *testDataReader = new Reader;
    testDataReader->readDataFromFile(fileName + "_test.dat");
    testLinhas = testDataReader->cols;
    testColunas = testDataReader->rows;
    double **testData = functions::transposed_matrix(testDataReader->matrix, testDataReader->rows, testDataReader->cols);
    delete testDataReader;
    
    Reader *testLabelReader = new Reader;
    testLabelReader->readDataFromFile(fileName + "_test.cls");
    double *testLabels = testLabelReader->vector;
    delete testLabelReader;
	
	SVMMultiStage *multi = new SVMMultiStage(linhas, colunas);
	multi->loadData(data, labels);
	multi->slackness = slackness;
	multi->run();
	
	// Test the solution for the excluded points
	errorCounter = 0;
	vector<double> point;
	for (int index = 1; index <= testLinhas; index++) {
		point.clear();
		point.push_back(0);
		for (int j = 1; j <= testColunas; j++) {
			point.push_back(testData[index][j]);
		}
		
		if (functions::test_point(multi->w, multi->gamma, point, testLabels[index]) == false) {
			errorCounter++;
		}
	}
	
	delete multi;
	
	doubleRet = 100 * (1 - (double)errorCounter/testLinhas);
	
	return doubleRet;
}

bool functions::test_point(const vector<double> w, const double gamma, const vector<double> x, int label) {
    // Class A (label -1): w'x > gamma
    // Class B (label +1): w'x < gamma
    
    bool ret = false;
    double wx = 0;
    
    for (int i = 1; i <= x.size(); i++) {
        wx += w[i] * x[i];
    }
    
    if (label == -1) {
        ret = (wx < gamma);
    } else if (label == 1) {
        ret = (wx > gamma);
    }
    
    return ret;
}

typedef struct {
	double x;
	double y;
} Point;

void generateSampleData(int classSize) {
	vector<Point> classA;
	vector<Point> classB;
	
	do {
		Point a;
		a.x = - 2 * ((double) arc4random() / UINT_MAX);
		a.y = 2 * ((double) arc4random() / UINT_MAX) - 1;
		
		if (a.y - a.x >= 1) {
			classA.push_back(a);
		}
	} while (classA.size() < classSize);
	
	do {
		Point b;
		b.x = 2 * ((double) arc4random() / UINT_MAX);
		b.y = 2 * ((double) arc4random() / UINT_MAX) - 1;
		
		if (b.y - b.x <= -1) {
			classB.push_back(b);
		}
	} while (classB.size() < classSize);
	
	ofstream data ("/Users/leoformaggio/Desktop/sample_data.dat");
    if (data.is_open()) {
        for (int i = 0; i < classSize; i++) {
			data << classA[i].x << " ";
        }
		for (int i = 0; i < classSize; i++) {
			data << classB[i].x << " ";
        }
		data << endl;
		
		for (int i = 0; i < classSize; i++) {
			data << classA[i].y << " ";
        }
		for (int i = 0; i < classSize; i++) {
			data << classB[i].y << " ";
        }
		data << endl;
		
        data.close();
    } else {
        cout << "Unable to open file\n";
    }
	
	ofstream cls ("/Users/leoformaggio/Desktop/sample_data.cls");
    if (cls.is_open()) {
        for (int i = 0; i < classSize; i++) {
			cls << -1 << " ";
        }
		
		for (int i = 0; i < classSize; i++) {
			cls << 1 << " ";
        }
		cls << endl;
		
        cls.close();
    } else {
        cout << "Unable to open file\n";
    }
	
	ofstream points ("/Users/leoformaggio/Desktop/sample.txt");
    if (points.is_open()) {
        for (int i = 0; i < classSize; i++) {
			points << classA[i].x << ", ";
			points << classA[i].y << "; ";
        }

		for (int i = 0; i < classSize; i++) {
			points << classB[i].x << ", ";
			points << classB[i].y << "; ";
        }
		points << endl;
		
        points.close();
    } else {
        cout << "Unable to open file\n";
    }
}
