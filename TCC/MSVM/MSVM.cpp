//
//  MSVM.cpp
//  TCC
//
//  Created by Leonardo Formaggio on 5/28/13.
//  Copyright (c) 2013 Fingertips. All rights reserved.
//

#include "MSVM.h"
#include "math.h"
#include <float.h>

#pragma mark - Constructors / Destructors

MSVM::MSVM() : GLPKProblem() {
    
}

MSVM::MSVM(int _rows, int _columns) : GLPKProblem(_rows + 1, 2*_columns + 2) {
	this->setOriginalDataSize(_rows, _columns);
	this->buildObjectiveFunction();
    this->setOptimizationDirection(GLP_MIN);
	this->setColumnBounds(1, columns - 2, GLP_LO, 0.0, 0.0);
    this->setColumnBounds(columns - 1, GLP_FR, 0.0, 0.0);
	this->setColumnBounds(columns, GLP_LO, 0.0, 0.0);
    this->setRowBounds(1, rows - 1, GLP_LO, 0.0, 0.0);
	this->setRowBounds(rows, GLP_FX, 1.0, 0.0);
}

MSVM::~MSVM() {
	
}

#pragma mark - Private

template <class T>
T MSVM::get_median(std::vector<T> v) {
	T val = 0;
	size_t size = v.size();
	std::sort(v.begin(), v.end());
	if (size % 2 == 0) {
		val = (v[size/2] + v[size/2-1]) / 2;
	} else {
		val = v[size/2];
	}
	return val;
}

void MSVM::determine_medians(double **data, double *labels) {
	xma = std::vector<double>();
	xmb = std::vector<double>();
	
	for (int j = 1; j <= dataColumns; j++) {
		std::vector<double> xa = std::vector<double>();
		std::vector<double> xb = std::vector<double>();
		
		for (int i = 1; i <= dataRows; i++) {
			if (labels[i] == -1) {
				xa.push_back(data[i][j]);
			} else {
				xb.push_back(data[i][j]);
			}
		}
		
		xma.push_back(this->get_median(xa));
		xmb.push_back(this->get_median(xb));
	}
}

#pragma mark - Classic SVM Methods

void MSVM::loadData(double **data, double *labels) {
	double **retMatrix;
	
	this->determine_medians(data, labels);
    
    // allocate memory for the GLPK matrix
    retMatrix = (double **)malloc((rows+1) * sizeof(double *));
    for (int i=1; i <= rows; i++) {
        retMatrix[i] = (double *)malloc((columns+1) * sizeof(double));
    }
    
    // set 0.0 for all entries
    for (int i=1; i <= rows; i++) {
        for (int j=1; j <= columns; j++) {
            retMatrix[i][j] = 0.0;
        }
    }
    
    // pass data to retMatrix
    for (int i=1; i <= dataRows; i++) {
        double label = labels[i];
        for (int j=1; j <= dataColumns; j++) {
            // w^{+}
            retMatrix[i][j] = label * data[i][j];
            // w^{-}
            retMatrix[i][j+dataColumns] = - label * data[i][j];
        }
    }
	
	for (int j=1; j <= dataColumns; j++) {
		// w^{+}
		retMatrix[rows][j] = xmb[j-1] - xma[j-1];
		// w^{-}
		retMatrix[rows][j+dataColumns] = xma[j-1] - xmb[j-1];
	}
    
    for (int i = 1; i <= dataRows; i++) {
        double label = labels[i];
		// gamma
        retMatrix[i][columns-1] = -label;
		// csi
		retMatrix[i][columns] = -1;
    }
    
	this->loadMatrix(retMatrix);
	
	free(retMatrix);
}

void MSVM::buildObjectiveFunction() {
	double *coef = (double *)malloc((columns+1)*sizeof(double));
    for (int i=1; i < columns; i++) {
        coef[i] = 1.0;
    }
    coef[columns] = 0.0;
	coef[columns-1] = 0.0;
	
	this->setObjectiveFunction(coef);
	
	free(coef);
}

#pragma mark - Auxiliary Methods

void MSVM::setOriginalDataSize(int rows, int columns) {
    dataRows = rows;
    dataColumns = columns;
}

void MSVM::printSolution() {
    // Imprime o resultado
	//    printf("z = %g\n", objective);
    for (int i=1; i < w.size(); i++) {
        printf("w[%d] = %g\n", i, w[i]);
    }
}

void MSVM::run() {
	this->solve();
	this->getSolution();
	this->getHyperplane();
}

void MSVM::getHyperplane() {
    w.clear();
	w.push_back(0);
	
    nonZeros = 0;
    
    for (int i=1; i<=dataColumns; i++) {
		w.push_back(solution[i] - solution[i+dataColumns]);
		
        if (fabs(w[i]) > DBL_EPSILON) {
            nonZeros++;
        }
    }
	gamma = solution[columns-1];
}