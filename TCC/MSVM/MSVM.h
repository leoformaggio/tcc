//
//  MSVM.h
//  TCC
//
//  Created by Leonardo Formaggio on 5/28/13.
//  Copyright (c) 2013 Fingertips. All rights reserved.
//

#ifndef __TCC__MSVM__
#define __TCC__MSVM__

#include <iostream>
#include "GLPKProblem.h"

class MSVM : public GLPKProblem {
private:
	std::vector<double> xma; // -1
	std::vector<double> xmb; // +1
	
	template <class T>
	T get_median(std::vector<T> v);
	void determine_medians(double **data, double *labels);
protected:
public:
	std::vector<double> w;
	double gamma;
	
    // Constructor
    MSVM();
    MSVM(int _rows, int _columns);
    // Destructor
    virtual ~MSVM();
    
	// Original data dimension
    int dataRows;
    int dataColumns;
    
    void setOriginalDataSize(int rows, int columns);
    
	void loadData(double **data, double *labels);
	void buildObjectiveFunction();
	void run();
    void getHyperplane();
	
	
	void printSolution();
};

#endif
