//
//  main.c
//  TCC
//
//  Created by Leonardo Formaggio on 5/23/11.
//  Copyright 2011 Fingertips. All rights reserved.
//

#include <string>
#include <iostream>
#include <numeric>
#include "CSVM.h"
#include "MSVM.h"
#include "SVMClassic.h"
#include "SVMMultiStage.h"
#include "Test.h"

using namespace std;

void compare(string data, int count);

int main (int argc, const char * argv[]) {
	compare("galaxy-dim", 3);
    return 0;
}

void compare_pequeno() {
	vector<float> slacks;
	for (float slack = 0.0; slack <= 1.0; slack += 0.01) {
		slacks.push_back(slack);
	}
	
	cout << "CSVM	MSVM	SVM	MSSVM" << endl;
	
	for (int i = 1; i <= 20; i++) {
		string data = "wdbc_pequeno" + to_string(i);
		
		Test<CSVM> csvm_test = Test<CSVM>(data);
		Test<MSVM> msvm_test = Test<MSVM>(data);
		Test<SVMClassic> svm_test = Test<SVMClassic>(data);
		
		map<float, double> result = functions::cross_validation(data, slacks, 10);
		map<float, double>::iterator it = std::max_element(result.begin(), result.end(), [](const pair<float, double>& p1, const pair<float, double>& p2) {
			return p1.second < p2.second;
		});
		Test<SVMMultiStage> svmms_test = Test<SVMMultiStage>(data, it->first);
		
		cout << csvm_test.run() << "	" << msvm_test.run() << "	" << svm_test.run() << "	" << svmms_test.run() << endl;
	}
}

void compare(string db, int count) {
	vector<float> slacks;
	for (float slack = 0.0; slack <= 1.0; slack += 0.01) {
		slacks.push_back(slack);
	}
	
	cout << "Database	CSVM	MSVM	SVM	MSSVM	Slack" << endl;
	
	for (int i = 1; i <= count; i++) {
		string data = db;
		if (count > 0) data += to_string(i);
		
		Test<CSVM> csvm_test = Test<CSVM>(data);
		Test<MSVM> msvm_test = Test<MSVM>(data);
		Test<SVMClassic> svm_test = Test<SVMClassic>(data);
		
		map<float, double> result = functions::cross_validation(data, slacks, 10);
		map<float, double>::iterator it = std::max_element(result.begin(), result.end(), [](const pair<float, double>& p1, const pair<float, double>& p2) {
			return p1.second < p2.second;
		});
		Test<SVMMultiStage> svmms_test = Test<SVMMultiStage>(data, it->first);
		
		double csvm_result = csvm_test.run();
		double msvm_result = msvm_test.run();
		double svm_result = svm_test.run();
		double svmms_result = svmms_test.run();
		
		cout << data << "	";
		cout << csvm_result << "	";
		cout << msvm_result << "	";
		cout << svm_result << "	";
		cout << svmms_result << "	";
		cout << svmms_test.slack() << endl;
	}
}

void cross_validation_pequeno() {
	vector<float> slacks;
	for (float slack = 0.0; slack <= 1.0; slack += 0.01) {
		slacks.push_back(slack);
	}
	
	map<float, double> results = map<float, double>();
	
	cout << "slack	hits" << endl;
	for (int i = 1; i <= 20; i++) {
		string data = "wdbc_pequeno" + to_string(i);
		map<float, double> result = functions::cross_validation(data, slacks, 10);
		map<float, double>::iterator it;
		for (it = result.begin(); it != result.end(); it++) {
			results[it->first] += it->second / 20;
		}
	}
	
	map<float, double>::iterator it;
	for (it = results.begin(); it != results.end(); it++) {
		cout << it->first << "	" << it->second << endl;
	}
}
